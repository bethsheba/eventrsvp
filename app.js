import express from "express";
import path from "path";
import fs from "fs";
import util from "util";
import mongoose from "mongoose";

mongoose.connect("mongodb://localhost/test", { useNewUrlParser: true });

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
  // we're connected!
});

const RSVP = mongoose.Schema({
  name: {
    type: String,
  },
  email: {
    type: String,
  },
  attending: {
    type: Boolean,
  },
  notAttending: {
    type: Boolean,
  },
  numOfGuests: {
    type: Number,
  },
});

export const Response = mongoose.model("response", RSVP);

const readDirAsync = util.promisify(fs.readdir);

const __dirname = path.resolve(path.dirname("."));
const staticDirectory = path.resolve(__dirname, "uploads");


const app = express();

//PUG
app.set("view engine", "pug");

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
app.use(express.json()); // allows me to read JSON from a POST or PUT request
app.use(express.urlencoded({ extended: false }));
// TODO serve built react files
app.use(express.static(staticDirectory));

const port = 3000;

app.get("/", async (req, res) => {
  const files = await readDirAsync("./uploads");
  res.render("index", { data: files });
});

app.get("/guestlist", async (req, res) => {
  const guestList = await Response.find();
  console.log(guestList)

  // const attending = await Response.({ attending: "true" });
  // const notAttending = await Response.findOne({ notAttending: "false" });
  // const attending1 = await Response.find().where('attending').all(false);
  // const attendees = Response.find({ attending: /^true/ });
  res.render("guestlist",  {
    attending: guestList.filter((RSVP) => RSVP.attending),
    notAttending: guestList.filter((RSVP) => !RSVP.attending),
  });

});

app.post("/reply", async (req, res) => {
  const data = req.body;
  console.log(data);
  const newGuest = new Response(data);
  await newGuest.save();
  res.render("reply");
});


app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);

process.on("unhandledRejection", (reason, p) => {
  console.error("Unhandled Rejection at:", p, "reason:", reason);
  // send entire app down. Process manager will restart it
  process.exit(1);
});
